
import cdsutils
import lscparams
from gpstime import gpstime
tconvert = gpstime.tconvert
import numpy
import time
from guardian import GuardState, GuardStateDecorator
import fast_ezca as fez
import math

##################################################
# check for lock
##################################################

def is_locked(dof):
    if dof in ['XARM_GREEN', 'YARM_GREEN']:
        '''lockedStates = ['LOCKED_NO_SLOW',
                        'LOCKED_W_SLOW_FEEDBACK',
                        'OFFLOAD_GREEN_WFS',
                        'LOCKED_NO_SLOW_NO_WFS',
                        'NO_SLOW_W_ETM_WFS',
                        'ENGAGE_WFS',
                        'LOCKED_SLOW_NO_WFS',
                        'LOCKED_RED',
                        'LOCKED_SLOW_W_ETM_WFS',
                        'LOCKED_SLOW_W_GR_WFS_PUM',
                        'NO_SLOW_W_ETM_WFS',
                        'NO_SLOW_W_GR_WFS']'''
        lockedStates = ['End Locked',
                        'Slow Engaged',
                        'Transition',
                        'Red Locked'
                        ];
        arm = dof[0]
        if (   ezca.read('ALS-%s_LOCK_STATE'%arm, as_string=True) not in lockedStates
            or ezca.read('GRD-SUS_ETM%s_STATE_S'%arm, as_string=True) == 'TRIPPED'):
            return False
            log('%s arm is not locked in green'%arm)
        else:
            return True
    elif dof == 'YARM':
        return ezca['LSC-YARM_FM_TRIG_MON'] >= 1
    elif dof == 'XARM':
        return ezca['LSC-XARM_FM_TRIG_MON'] >= 1
    elif dof == 'PRX':
        return cdsutils.avg(1, 'ASC-POP_A_NSUM_OUTPUT') > lscparams.thresh['PRXY']['LOCKED']
    elif dof == 'PRY':
        return cdsutils.avg(1, 'ASC-POP_A_NSUM_OUTPUT') > lscparams.thresh['PRXY']['LOCKED']
    elif dof == 'SRX':
        return ezca['LSC-SRCL_TRIG_MON'] >= 1
    elif dof == 'SRY':
        return ezca['LSC-SRCL_TRIG_MON'] >= 1
    elif dof == 'PRMI':
        MichMon = ezca['LSC-MICH_TRIG_MON']
        log(MichMon)
        PrclMon = ezca['LSC-PRCL_TRIG_MON']
        if (ezca['LSC-MICH_TRIG_MON'] and ezca['LSC-PRCL_TRIG_MON']):
            return True
    elif dof == 'MICH_DARK':
        return cdsutils.avg(1, 'ASC-AS_A_DC_NSUM_OUTPUT') <= lscparams.thresh['MICHDARK']['LOCKED']
    elif dof == 'MICH_BRIGHT':
        return cdsutils.avg(1, 'ASC-AS_A_DC_NSUM_OUTPUT') >= lscparams.thresh['MICHBRIGHT']['LOCKED']
    elif dof == 'DRMI':
        MichMon = ezca['LSC-MICH_TRIG_MON']
        PrclMon = ezca['LSC-PRCL_TRIG_MON']
        SrclMon = ezca['LSC-SRCL_TRIG_MON']
        if (MichMon > 0.5) and (PrclMon > 0.5) and (SrclMon > 0.5):
            # We're still locked and triggered, so return True
            return True
    elif dof == 'IMC':
        trans_pd_lock_threshold = 75 # New val after calib
        return ((ezca['IMC-MC2_TRANS_NSUM_OUTPUT']/ezca['IMC-PWR_IN_OUTPUT'] >= trans_pd_lock_threshold)
        and (ezca['IMC-PWR_IN_OUT16'] > 0.1))
    elif dof == 'OMC':
        return ezca['OMC-LSC_LOCK_TRIGGER_LOCKMON']
    elif dof == 'Full_IFO':
        if ezca['LSC-TR_X_NORM_INMON'] >=500:
            return True
        else:
            return False
    elif dof == 'SQZ':
        return ezca.read('GRD-SQZ_LOCK_STATE_S', as_string = True)=='LOCKED_SEED'
    else:
        log('checking lock of unrecognized dof')
        return None



#################################################
# check for error conditions
#################################################

def WFS_DC_centering_servos_OK(port):
    okayFlag = True # initialze
    if 'IMC' in port:
        for ab in ['A','B']:
            for dof in ['PIT','YAW']:
                if abs(ezca['IMC-WFS_{}_DC_{}_OUTPUT'.format(ab, dof)]) > 0.5:
                    notify('IMC WFS not centered')
                    okayFlag = False
    else:
        if port == 'REFL':
            servos = [1,2]
        elif port == 'AS':
            servos = [3,4]
        elif port == 'REFL_AS':
            servos = [1,2,3,4]

        if ezca['ASC-WFS_GAIN'] < 0.1:
            notify('ASC MASTER GAIN')

        for py in ['P','Y']:
            for servo in servos:
                if (abs(ezca['ASC-DC%s_%s_OUTPUT'%(servo,py)]) >= ezca['ASC-DC%s_%s_LIMIT'%(servo,py)]):
                    okayFlag = False

    return okayFlag


def REFL_PD_OK():
    return ezca['LSC-REFL_A_LF_OUTPUT'] <= 100


def PRXY_oscillating():
    return cdsutils.avg(1, 'LSC-POPAIR_A_LF_OUT_DQ', True)[1] >= lscparams.thresh['PRXY']['OSCILLATING']


def PSL_ready():
    flag = True
    if ezca['PSL-PMC_LOCK_ON'] != -30000:
        notify("PMC unlocked")
        flag = False
    elif ezca['PSL-PERISCOPE_A_DC_ERROR_FLAG'] != 0:
        notify("PSL periscope PD error (low light?)")
        flag = False
    if ezca['PSL-FSS_AUTOLOCK_STATE'] != 4:
        notify("FSS unlocked")
        flag = False

    return flag

def iss_ok():
    return (ezca['PSL-ISS_LOOP_STATE_OUTPUT'] > 30000)


def prg_okay(prg):
    return (ezca['LSC-PR_GAIN_OUT16'] > prg)


#################################################
# check for convergence
#################################################

def asc_convergence_checker(loopsList, Pit_thresholds, Yaw_thresholds):
    ConvergenceFlag = True
    for ii in range(len(loopsList)):
        if abs(ezca['ASC-{}_P_OUT16'.format(loopsList[ii])]) > Pit_thresholds[ii]:
            ConvergenceFlag = False
            log('{0} Pit not converged, thresh={1}'.format(loopsList[ii], Pit_thresholds[ii]))
        if abs(ezca['ASC-{}_Y_OUT16'.format(loopsList[ii])]) > Yaw_thresholds[ii]:
            ConvergenceFlag = False
            log('{0} Yaw not converged, thresh={1}'.format(loopsList[ii], Yaw_thresholds[ii]))
    return ConvergenceFlag

def asc_err_convergence_checker(loopsList, Pit_thresholds, Yaw_thresholds):
    ConvergenceFlag = True
    for ii in range(len(loopsList)):
        if abs(ezca['ASC-{}_P_INMON'.format(loopsList[ii])]) > Pit_thresholds[ii]:
            ConvergenceFlag = False
            log('{0} Pit error signal not converged, thresh={1}'.format(loopsList[ii], Pit_thresholds[ii]))
        if abs(ezca['ASC-{}_Y_INMON'.format(loopsList[ii])]) > Yaw_thresholds[ii]:
            ConvergenceFlag = False
            log('{0} Yaw error signal not converged, thresh={1}'.format(loopsList[ii], Yaw_thresholds[ii]))
    return ConvergenceFlag

def dither_convergence_checker(loopsList, thresholds):
	ConvergenceFlag = True
	for ii in range(len(loopsList)):
		if abs(ezca['ASC-ADS_%s_DOF_INMON'%loopsList[ii]]) > thresholds[ii]:
			ConvergenceFlag = False
	return ConvergenceFlag

def alsWfs_convergence_checker(dofList, Pit_thresholds, Yaw_thresholds, arm):
    ConvergenceFlag = True
    nTest=32
    ct=0
    while ct  < nTest:
        if ConvergenceFlag == False:
            break
        for ii in range(len(dofList)):
            Pit_value = abs(ezca['ALS-%s_WFS_DOF_%i_P_OUT16'%(arm, dofList[ii])])
            Yaw_value = abs(ezca['ALS-%s_WFS_DOF_%i_Y_OUT16'%(arm, dofList[ii])])
            if Pit_value > Pit_thresholds[ii]:
        	    ConvergenceFlag = False
        	    log('{0} Pit not converged, thresh={1}, current={2:.1f}'.format(dofList[ii], Pit_thresholds[ii], Pit_value))
            if Yaw_value > Yaw_thresholds[ii]:
                ConvergenceFlag = False
                log('{0} Yaw not converged, thresh={1}, current={2:.1f}'.format(dofList[ii], Yaw_thresholds[ii], Yaw_value))
        ct += 1
    return ConvergenceFlag

def ads_convergence_checker(loopsList, Pit_thresholds, Yaw_thresholds):
    ConvergenceFlag = True
    for ii in range(len(loopsList)):
        if abs(ezca['ASC-ADS_PIT{0}_DOF_OUT16'.format(loopsList[ii])]) > Pit_thresholds[ii]:
            ConvergenceFlag = False
            log('{0} Pit not converged, thresh={1}'.format(loopsList[ii], Pit_thresholds[ii]))
        if abs(ezca['ASC-ADS_YAW{0}_DOF_OUT16'.format(loopsList[ii])]) > Yaw_thresholds[ii]:
            ConvergenceFlag = False
            log('{0} Yaw not converged, thresh={1}'.format(loopsList[ii], Yaw_thresholds[ii]))
    return ConvergenceFlag


#################################################
# check input power (backup)
#################################################
#this is what we want, we don't want to change the input sliders durring power up because that can cause locklosses
def IMC_power_adjust_func():
    # at 2.0 Watts input power the nominal gain on IMC Fast Gain should be 0 dB
    nominalInputPower = 1.85
    gain_scaler = lscparams.IMC_FASTGAIN_PowerUp_Scaler # dB, this is to avoid saturations in the IMC Board
    calc_gain = 20.0*math.log10(nominalInputPower / ezca['IMC-PWR_IN_OUTMON']) + gain_scaler
    calc_gain = min(calc_gain, 31)

    # it only changes the power if there is a difference of 1 dB.
    # This is intended to prevent repeated 1 dB gain changes
    #  if we are on the edge between two gain settings with the input power.

    diff_gain = calc_gain - ezca['IMC-REFL_SERVO_FASTGAIN']

    if abs(diff_gain) >= 2.0:
        R_diff_gain=round(diff_gain)
        log('power_adjust is changing IMC Servo Board FASTGAIN from {} by {}'.format( ezca['IMC-REFL_SERVO_FASTGAIN'], R_diff_gain) )
        ezca['IMC-REFL_SERVO_FASTGAIN'] += R_diff_gain # Has to be +=, this was causing locklosses Dec 21, 2018

        if abs(ezca['LSC-MCL_GAIN']) > 1e-9:
            ezca['LSC-MCL_GAIN'] *= 10**(R_diff_gain/20.0)

        elif abs(ezca['IMC-MCL_GAIN']) > 1e-9:
            ezca['IMC-MCL_GAIN'] *= 10**(R_diff_gain/20.0)

        time.sleep(0.2)
    return

def adjust_radiation_pressure_compensation():
    loops = ['CHARD_P', 'CHARD_Y','DHARD_P', 'DHARD_Y']# , 'CSOFT_P', 'DSOFT_P']
    # Don't do SOFT loops JCD 7Feb2019
    #adjust rpc according to input power in steps
    for loop in loops:
        if ezca['IMC-PWR_IN_OUT16'] <= 14:
            ezca['ASC-RPC_%s_GAIN'%(loop)] = lscparams.asc_gains['RPC'][loop]['14']
        elif ezca['IMC-PWR_IN_OUT16'] <= 19:
            ezca['ASC-RPC_%s_GAIN'%loop] = lscparams.asc_gains['RPC'][loop]['19']
        elif ezca['IMC-PWR_IN_OUT16'] <= 22.5:
            ezca['ASC-RPC_%s_GAIN'%loop] = lscparams.asc_gains['RPC'][loop]['22.5']
        elif ezca['IMC-PWR_IN_OUT16'] <= 24.5:
            ezca['ASC-RPC_%s_GAIN'%loop] = lscparams.asc_gains['RPC'][loop]['24.5']
        elif ezca['IMC-PWR_IN_OUT16'] <= 27.2:
            ezca['ASC-RPC_%s_GAIN'%loop] = lscparams.asc_gains['RPC'][loop]['27.2']
        elif ezca['IMC-PWR_IN_OUT16'] <= 29.5:
            ezca['ASC-RPC_%s_GAIN'%loop] = lscparams.asc_gains['RPC'][loop]['29.5']
        elif ezca['IMC-PWR_IN_OUT16'] <= 33:
            ezca['ASC-RPC_%s_GAIN'%loop] = lscparams.asc_gains['RPC'][loop]['33']
        elif ezca['IMC-PWR_IN_OUT16'] <= 36.5:
            ezca['ASC-RPC_%s_GAIN'%loop] = lscparams.asc_gains['RPC'][loop]['36.5']
        elif ezca['IMC-PWR_IN_OUT16'] <= 40:
            ezca['ASC-RPC_%s_GAIN'%loop] = lscparams.asc_gains['RPC'][loop]['40']
    return

#################################################
# SUS config
#################################################

sus_config = {
    'ALS_ONLY':{'ETMY':'ALIGNED',
                'ITMY':'ALIGNED',
                'ETMX':'ALIGNED',
                'ITMX':'ALIGNED',
                'PR2':'ALIGNED',
                'SR2':'ALIGNED',
                'PRM':'MISALIGNED',
                'SRM':'MISALIGNED'},
    'PRMI_W_ALS':{'ETMY':'ALIGNED',
                'ITMY':'ALIGNED',
                'ETMX':'ALIGNED',
                'ITMX':'ALIGNED',
                'PR2':'ALIGNED',
                'SR2':'ALIGNED',
                'PRM':'ALIGNED',
                'SRM':'MISALIGNED'},
    'FULL_LOCK':{'ETMY':'ALIGNED',
                'ITMY':'ALIGNED',
                'ETMX':'ALIGNED',
                'ITMX':'ALIGNED',
                'PR2':'ALIGNED',
                'SR2':'ALIGNED',
                'PRM':'ALIGNED',
                'SRM':'ALIGNED'},
    'MICH':{'ETMY':'MISALIGNED',
                'ITMY':'ALIGNED',
                'ETMX':'MISALIGNED',
                'ITMX':'ALIGNED',
                'PR2':'ALIGNED',
                'SR2':'ALIGNED',
                'PRM':'MISALIGNED',
                'SRM':'MISALIGNED'},
    'PRX':{'ETMY':'MISALIGNED',
                'ITMY':'MISALIGNED',
                'ETMX':'MISALIGNED',
                'ITMX':'ALIGNED',
                'PR2':'ALIGNED',
                'SR2':'ALIGNED',
                'PRM':'ALIGNED',
                'SRM':'MISALIGNED'},
    'PRY':{'ETMY':'MISALIGNED',
                'ITMY':'ALIGNED',
                'ETMX':'MISALIGNED',
                'ITMX':'MISALIGNED',
                'PR2':'ALIGNED',
                'SR2':'ALIGNED',
                'PRM':'ALIGNED',
                'SRM':'MISALIGNED'},
    'SRX':{'ETMY':'MISALIGNED',
                'ITMY':'MISALIGNED',
                'ETMX':'MISALIGNED',
                'ITMX':'ALIGNED',
                'PR2':'ALIGNED',
                'SR2':'ALIGNED',
                'PRM':'MISALIGNED',
                'SRM':'ALIGNED'},
    'SRY':{'ETMY':'MISALIGNED',
                'ITMY':'ALIGNED',
                'ETMX':'MISALIGNED',
                'ITMX':'MISALIGNED',
                'PR2':'ALIGNED',
                'SR2':'ALIGNED',
                'PRM':'MISALIGNED',
                'SRM':'ALIGNED'},
    'PRMI':{'ETMY':'MISALIGNED',
                'ITMY':'ALIGNED',
                'ETMX':'MISALIGNED',
                'ITMX':'ALIGNED',
                'PR2':'ALIGNED',
                'SR2':'ALIGNED',
                'PRM':'ALIGNED',
                'SRM':'MISALIGNED'},
    'DRMI':{'ETMY':'MISALIGNED',
                'ITMY':'ALIGNED',
                'ETMX':'MISALIGNED',
                'ITMX':'ALIGNED',
                'PR2':'ALIGNED',
                'SR2':'ALIGNED',
                'PRM':'ALIGNED',
                'SRM':'ALIGNED'},
    'XARM':{'ETMY':'MISALIGNED',
                'ITMY':'MISALIGNED',
                'ETMX':'ALIGNED',
                'ITMX':'ALIGNED',
                'PR2':'ALIGNED',
                'SR2':'ALIGNED',
                'PRM':'MISALIGNED',
                'SRM':'MISALIGNED'},
    'YARM':{'ETMY':'ALIGNED',
                'ITMY':'ALIGNED',
                'ETMX':'MISALIGNED',
                'ITMX':'MISALIGNED',
                'PR2':'ALIGNED',
                'SR2':'ALIGNED',
                'PRM':'MISALIGNED',
                'SRM':'MISALIGNED'},
    'SINGLE_BOUNCE_Y':{'ETMY':'MISALIGNED',
                'ITMY':'ALIGNED',
                'ETMX':'MISALIGNED',
                'ITMX':'MISALIGNED',
                'PR2':'ALIGNED',
                'SR2':'ALIGNED',
                'PRM':'MISALIGNED',
                'SRM':'MISALIGNED'},
    'SQZ_SINGLE_BOUNCE':{'ETMY':'MISALIGNED',
                'ITMY':'MISALIGNED',
                'ETMX':'MISALIGNED',
                'ITMX':'MISALIGNED',
                'PR2':'MISALIGNED',
                'SR2':'MISALIGNED',
                'PRM':'MISALIGNED',
                'SRM':'ALIGNED'},
    }

def set_sus_config(config, nodes):
    log(sus_config[config])
    for sus in sus_config[config]:
        alignment = sus_config[config][sus]
        nodename = 'SUS_%s'%(sus)
        nodes[nodename] = alignment

#################################################
# DECORATORS
#################################################

def get_subordinate_watchdog_check_decorator(nodes):
    class subordinate_watchdog_check(GuardStateDecorator):
        """Check that any watchdog of a subordinate node has not tripped."""
        def pre_exec(self):
            tripped_nodes = []
            for node in nodes:
                if ('WATCHDOG_TRIPPED' in node.state):
                    tripped_nodes.append(node)

            if tripped_nodes:  # if list not empty, notify
                notify('WATCHDOG TRIPPED')

    return subordinate_watchdog_check


def unstall_nodes(nodes):
    class unstall_decorator(GuardStateDecorator):
        def pre_exec(self):
            for node in nodes.get_stalled_nodes():
                # put a check that in is in done state
                if not node.NOTIFICATION:
                    log('Unstalling ' + node.name)
                    node.revive()
    return unstall_decorator


def assert_dof_locked_gen(dofs):
    class assert_dof_locked(GuardStateDecorator):
        def pre_exec(self):
            for dof in dofs:
                if not is_locked(dof):
                    if is_locked == None:
                        notify('Unrecognized dof')

                    else:
                        if SYSTEM == 'ISC_DRMI':
                            return 'DOWN'
                        elif SYSTEM == 'ISC_LOCK':
                            if dof == 'DRMI':
                                return 'LOCKLOSS_DRMI'
                            else:
                                return 'LOCKLOSS'
                        elif (SYSTEM == 'IMC_LOCK'
                            or SYSTEM == 'ALIGN_IFO'
                            or SYSTEM == 'OMC_LOCK'):
                            return 'DOWN'
                        elif (SYSTEM == 'ALS_XARM' or SYSTEM == 'ALS_YARM'):
                            return 'FAULT'
                        elif SYSTEM == 'VIOLIN_DAMPING':
                            return 'TURN_OFF_DAMPING_ALL'
                        else: # ALS_COMM and ALS_DIFF Full_IFO
                            return 'LOCKLOSS'
    return assert_dof_locked


class check_PSL(GuardStateDecorator):
    def pre_exec(self):
        if not PSL_ready():
            # Used by IMC node
            return 'FAULT'


def gen_check_WFS_DC(dofs):
    class check_WFS_DC(GuardStateDecorator):
        def pre_exec(self):
            for dof in dofs:
                if not WFS_DC_centering_servos_OK(dof):
                    break # Jnote
    return check_WFS_DC


class IMC_power_adjust(GuardStateDecorator):
    def pre_exec(self):
        IMC_power_adjust_func()


class iss_checker(GuardStateDecorator):
    """Decorator that jumps to OPEN_ISS if the ISS second loop output is too large."""
    def pre_exec(self):
        if not iss_ok():
            log('1st loop is saturated, and open')
            return 'OPEN_ISS'

##################################################
# LSC/ASC input and output matrices                 #
##################################################
# LSC input matrix for the majority
intrix = cdsutils.CDSMatrix('LSC-PD_DOF_MTRX',
		ramping = True,
		cols={'POP_A9I':1,
			'POP_A9Q':2,
			'POP_A45I':3,
			'POP_A45Q':4,
			'REFL_A9I':5,
			'REFL_A9Q':6,
			'REFL_A45I':7,
			'REFL_A45Q':8,
			'POPAIR_A9I':9,
			'POPAIR_A9Q':10,
			'POPAIR_A45I':11,
			'POPAIR_A45Q':12,
			'REFLAIR_A9I':13,
			'REFLAIR_A9Q':14,
			'REFLAIR_A45I':15,
			'REFLAIR_A45Q':16,
			'REFLAIR_B27I':17,
			'REFLAIR_B27Q':18,
			'REFLAIR_B135I':19,
			'REFLAIR_B135Q':20,
			'TRX':21,
			'TRY':22,
			'REFLSERVO_SLOW': 23,
			'ALS_COMM': 24,
			'ASAIR_ALF': 25,
			'TR_CARM':26,
			'TR_REFL9':27,
			'REFL_DC':28,
			'OMC_DC':29,
			'ASAIR_A45I':30,
			'ASAIR_A45Q':31
			},
		rows={'DARM':1,
			'CARM':2,
			'MICH':3,
			'PRCL':4,
			'SRCL':5,
			'MCL':6,
			'XARM':7,
			'YARM':8,
			'REFLBIAS':9
			}
		)

# LSC input matrix component in OMC model
intrix_OMCAS45 = cdsutils.CDSMatrix('LSC-ARM_INPUT_MTRX',
		ramping = True,
		cols={'OMCDC':1,
			'ASAIR_A45I':2,
			'ASAIR_A45Q':3,
			'ALS_DIFF':4,
			'REFL_DC':5
			},
		rows={'DARM':1,
			'CARM':2
			}
		)

# LSC output matrix for the majority
outrix = cdsutils.CDSMatrix('LSC-OUTPUT_MTRX',
			cols={'MICH':1,
				'PRCL':2,
				'SRCL':3,
				'MCL':4,
				'XARM':5,
				'YARM':6,
				'OSC1':7,
				'OSC2':8,
				'OSC3':9,
				'MICHFF':10,
				'SRCLFF':11
				},
			rows={'ETMX':1,
				'ETMY':2,
				'ITMX':3,
				'ITMY':4,
				'PRM':5,
				'SRM':6,
				'BS':7,
				'PR2':8,
				'SR2':9,
				'MC2':10,
                'AO3':11,
				}
			)

darmcarm_outrix = cdsutils.CDSMatrix('LSC-ARM_OUTPUT_MTRX',
		cols={'DARM':1, 'CARM':2},
		rows={'ETMX':1, 'ETMY':2, 'ITMX':3, 'ITMY':4}
		)


# LSC trigger matrix
trigrix = cdsutils.CDSMatrix('LSC-TRIG_MTRX',
				cols={'OMCDC':1,
				'POPAIR_B_RF18_I':2,
				'POPAIR_B_RF18_Q':3,
				'POPAIR_B_RF90_I':4,
				'POPAIR_B_RF90_Q':5,
				'ASAIR_B_RF18_I':6,
				'ASAIR_B_RF18_Q':7,
				'ASAIR_B_RF90_I':8,
				'ASAIR_B_RF90_Q':9,
				'REFLAIR_A_DC':10,
				'REFLAIR_B_DC':11,
				'POPAIR_A_DC':12,
				'POPAIR_B_DC':13,
				'ASAIR_A_DC':14,
				'ASAIR_B_DC':15,
				'REFL_A_DC':16,
				'POP_A_DC':17,
				'TRX':18,
				'TRY':19
				},
				rows={'DARM':1,
				 	'MICH':2,
					'PRCL':3,
					'SRCL':4,
					'CARM':5,
					'XARM':6,
					'YARM':7,
					'REFLBIAS':8
				}
			)

# ASC matrix definitions
asc_dofs = {	'INP1':1,
		'INP2':2,
		'PRC1':3,
		'PRC2':4,
		'MICH':5,
		'SRC1':6,
		'SRC2':7,
		'DHARD':8,
		'DHARD_B':20,
		'DSOFT_A':9,
		'DSOFT_B':21,
		'CHARD':10,
		'CHARD_B':19,
		'CSOFT_A':11,
		'CSOFT_B':22,
		'DC1':12,
		'DC2':13,
		'DC3':14,
		'DC4':15,
		'DC5':16,
		'DC6':17,
		'DC7':18
			}
asc_pds ={  'AS_A_RF45_I':1,
	    'AS_A_RF45_Q':2,
	    'AS_A_RF36_I':3,
	    'AS_A_RF36_Q':4,
	    'AS_B_RF45_I':5,
	    'AS_B_RF45_Q':6,
	    'AS_B_RF36_I':7,
	    'AS_B_RF36_Q':8,
	    'REFL_A_RF9_I':9,
	    'REFL_A_RF9_Q':10,
	    'REFL_A_RF45_I':11,
	    'REFL_A_RF45_Q':12,
	    'REFL_B_RF9_I':13,
	    'REFL_B_RF9_Q':14,
	    'REFL_B_RF45_I':15,
	    'REFL_B_RF45_Q':16,
	    'REFL_A_DC':17,
	    'REFL_B_DC':18,
	    'AS_A_DC':19,
	    'AS_B_DC':20,
	    'POP_A_DC':21,
	    'POP_B_DC':22,
	    'TRX_A':23,
	    'TRX_B':24,
	    'TRY_A':25,
	    'TRY_B':26,
	    'AS_C_DC':27,
            'IM4_TRANS':28,
            'POP_X_I':29,
            'POP_X_Q':30,
            'POP_X_DC':31,
            'AS_A_RF72_I':32,
            'AS_A_RF72_Q':33,
            'AS_B_RF72_I':34,
            'AS_B_RF72_Q':35,
		    }

asc_actuators = {'PRM':1,
		'PR2':2,
		'PR3':3,
		'BS':4,
		'ITMX':5,
		'ITMY':6,
		'ETMX':7,
		'ETMY':8,
		'SRM':9,
		'SR2':10,
		'SR3':11,
		'IM1':12,
		'IM2':13,
		'IM3':14,
		'IM4':15,
		'RM1':16,
		'RM2':17,
		'OM1':18,
		'OM2':19,
		'OM3':20,
		'TMSX':21,
		'TMSY':22,
                'PM1':23,
		}

# ASC input matrices
asc_intrix = {
    'PIT': cdsutils.CDSMatrix('ASC-INMATRIX_P',cols = asc_pds,rows = asc_dofs),
    'YAW': cdsutils.CDSMatrix('ASC-INMATRIX_Y',cols = asc_pds,rows = asc_dofs),
}

asc_dofs_extra = {	'DC6':28,
                    	'DC7':29,
                    	'ALS_X1':17,
			'ALS_X2':18,
			'ALS_X3':19,
			'ALS_Y1':20,
			'ALS_Y2':21,
			'ALS_Y3':22,
			'OSC1':23,
			'OSC2':24,
			'OSC3':25,
                   	'OSC4':26,
                   	'LOCKINOSC':27,
			}
asc_dofs.update(asc_dofs_extra) # update, to be correct for output matrix

# ASC output matrices
asc_outrix = {'PIT':cdsutils.CDSMatrix('ASC-OUTMATRIX_P',cols=asc_dofs,rows=asc_actuators),
              'YAW':cdsutils.CDSMatrix('ASC-OUTMATRIX_Y',cols=asc_dofs,rows=asc_actuators)}

#ADS matrices
asc_ads_sensors = {
    'DARM_CTRL': 1,
    'PRCL_CTRL': 2,
    'SRCL_CTRL': 3,
    'POPAIR_B_RF18_I': 4,
    'POP_A_LF': 5,
    'POPAIR_B_RF90_I': 6,
    'AS_A_NSUM': 7,
    'TR_X': 8,
    'TR_Y': 9,
    }

asc_ads_pit_dofs = {
    'PIT1': 1,
    'PIT2': 2,
    'PIT3': 3,
    'PIT4': 4,
    'PIT5': 5,
    'PIT6': 6,
    'PIT7': 7,
    'PIT8': 8,
    'PIT9': 9,
    'PIT10': 10,
    }

asc_ads_yaw_dofs = {
    'YAW1': 1,
    'YAW2': 2,
    'YAW3': 3,
    'YAW4': 4,
    'YAW5': 5,
    'YAW6': 6,
    'YAW7': 7,
    'YAW8': 8,
    'YAW9': 9,
    'YAW10': 10,
    }

# asc ads oscillators matrix
asc_ads_oscillators = {
    'OSC1': 1,
    'OSC2': 2,
    'OSC3': 3,
    'OSC4': 4,
    'OSC5': 5,
    'OSC6': 6,
    'OSC7': 7,
    'OSC8': 8,
    'OSC9': 9,
    'OSC10': 10,
    }

asc_ads_actuators_dither = {
    'PRM': 1,
    'PR2': 2,
    'PR3': 3,
    'BS':  4,
    'ITMX': 5,
    'ITMY': 6,
    'ETMX': 7,
    'ETMY': 8,
    'TMSX': 9,
    'TMSY': 10,
    'SRM': 11,
    'SR2': 12,
    'SR3': 13,
    }

asc_ads_actuators_out = {
    'IM3':   1,
    'IM4':   2,
    'PRM':   3,
    'PR2':   4,
    'PR3':   5,
    'BS':    6,
    'ITMX':  7,
    'ITMY':  8,
    'ETMX':  9,
    'ETMY': 10,
    'SRM':  11,
    'SR2':  12,
    'SR3':  13,
    }

asc_ads_intrix = {'PIT': cdsutils.CDSMatrix('ASC-ADS_PIT_SEN_MTRX', cols=asc_ads_sensors,rows=asc_ads_pit_dofs),
                  'YAW': cdsutils.CDSMatrix('ASC-ADS_YAW_SEN_MTRX', cols=asc_ads_sensors,rows=asc_ads_yaw_dofs)}
asc_ads_lotrix = {'PIT': cdsutils.CDSMatrix('ASC-ADS_LO_PIT_MTRX', cols=asc_ads_oscillators,rows=asc_ads_actuators_dither),
                  'YAW': cdsutils.CDSMatrix('ASC-ADS_LO_YAW_MTRX', cols=asc_ads_oscillators,rows=asc_ads_actuators_dither)}
asc_ads_outrix = {'PIT': cdsutils.CDSMatrix('ASC-ADS_OUT_PIT_MTRX', cols=asc_ads_pit_dofs,rows=asc_ads_actuators_out),
                  'YAW': cdsutils.CDSMatrix('ASC-ADS_OUT_YAW_MTRX', cols=asc_ads_yaw_dofs,rows=asc_ads_actuators_out)}

#sqz asc matrix
sqz_asc_sensors = {
    'AS_A_42': 1,
    'AS_B_42': 2,
    'OMC_A': 3,
    'OMC_B': 4,
    'AS_A_DC': 5,
    'AS_B_DC': 6
    }
sqz_asc_dofs = {	'POS':1,
		'ANG':2
			}
sqz_asc_actuators = { 'ZM1':1,
        'ZM2':2,
            }
sqz_asc_intrix = {'PIT':cdsutils.CDSMatrix('SQZ-ASC_INMATRIX_P',cols=sqz_asc_sensors,rows=sqz_asc_dofs),
                  'YAW':cdsutils.CDSMatrix('SQZ-ASC_INMATRIX_Y',cols=sqz_asc_sensors,rows=sqz_asc_dofs)}
sqz_asc_outrix = {'PIT':cdsutils.CDSMatrix('SQZ-ASC_OUTMATRIX_P',cols=sqz_asc_sensors,rows=sqz_asc_dofs),
                  'YAW':cdsutils.CDSMatrix('SQZ-ASC_OUTMATRIX_Y',cols=sqz_asc_sensors,rows=sqz_asc_dofs)}
##################################################
# Functions to aid with in-lock alignment        #
##################################################
def alignRef():
    class alignRefClass:
        ppr3=ezca['SUS-PR3_M1_OPTICALIGN_P_OFFSET']
        ppr2=ezca['SUS-PR2_M1_OPTICALIGN_P_OFFSET']
        pprm=ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET']
        pim4=ezca['SUS-IM4_M1_OPTICALIGN_P_OFFSET']
        pim3=ezca['SUS-IM3_M1_OPTICALIGN_P_OFFSET']
        psr2=ezca['SUS-SR2_M1_OPTICALIGN_P_OFFSET']
        psrm=ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET']
        pex =ezca['SUS-ETMX_M0_OPTICALIGN_P_OFFSET']
        pey =ezca['SUS-ETMY_M0_OPTICALIGN_P_OFFSET']
        pix =ezca['SUS-ITMX_M0_OPTICALIGN_P_OFFSET']
        piy =ezca['SUS-ITMY_M0_OPTICALIGN_P_OFFSET']
        ypr3=ezca['SUS-PR3_M1_OPTICALIGN_Y_OFFSET']
        ypr2=ezca['SUS-PR2_M1_OPTICALIGN_Y_OFFSET']
        yprm=ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET']
        yim4=ezca['SUS-IM4_M1_OPTICALIGN_Y_OFFSET']
        yim3=ezca['SUS-IM3_M1_OPTICALIGN_Y_OFFSET']
        ysr2=ezca['SUS-SR2_M1_OPTICALIGN_Y_OFFSET']
        ysrm=ezca['SUS-SRM_M1_OPTICALIGN_Y_OFFSET']
        yex =ezca['SUS-ETMX_M0_OPTICALIGN_Y_OFFSET']
        yey =ezca['SUS-ETMY_M0_OPTICALIGN_Y_OFFSET']
        yix =ezca['SUS-ITMX_M0_OPTICALIGN_Y_OFFSET']
        yiy =ezca['SUS-ITMY_M0_OPTICALIGN_Y_OFFSET']
    return alignRefClass()

def pr3spotmove(ref):
    ###########################
    # alog 28764
    ###########################

    # set 2
    #gppr3=0.086
    #gpprm=-13.07
    #gpim4=-305
    #gpsr2=-2.73
    #gpsrm=4.29
    #gpex=-0.7
    #gpey=-0.7
    #gpix=-0.45
    #gpiy=-0.35
    # set 1
    gppr3=0.07
    gpprm=-13.2
    gpim4=-325
    gpsr2=-2.39
    gpsrm=3.2
    gpex=-0.7
    gpey=-0.7
    gpix=-0.45
    gpiy=-0.35
    # set 1 yaw
    gypr3=0.028
    gyprm=11.07
    gyim4=61.22
    gysr2=2.22
    gysrm=2.74
    gyex=-0.5
    gyey=+0.5
    gyix=+0.32
    gyiy=-0.31
    #
    rpprm=ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET'];
    gp=1.0/gpprm*(rpprm-ref.pprm)
    ezca['SUS-PR3_M1_OPTICALIGN_P_OFFSET']  = ref.ppr3 + gppr3*gp
    #ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET']  = ref.pprm + gpprm*gp
    ezca['SUS-IM4_M1_OPTICALIGN_P_OFFSET']  = ref.pim4 + gpim4*gp
    ezca['SUS-SR2_M1_OPTICALIGN_P_OFFSET']  = ref.psr2 + gpsr2*gp
    ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET']  = ref.psrm + gpsrm*gp
    ezca['SUS-ETMX_M0_OPTICALIGN_P_OFFSET'] = ref.pex  + gpex *gp
    ezca['SUS-ETMY_M0_OPTICALIGN_P_OFFSET'] = ref.pey  + gpey *gp
    ezca['SUS-ITMX_M0_OPTICALIGN_P_OFFSET'] = ref.pix  + gpix *gp
    ezca['SUS-ITMY_M0_OPTICALIGN_P_OFFSET'] = ref.piy  + gpiy *gp
    #
    ryprm=ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET'];
    gy=1.0/gyprm*(ryprm-ref.yprm)
    ezca['SUS-PR3_M1_OPTICALIGN_Y_OFFSET']  = ref.ypr3 + gypr3*gy
    #ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET']  = ref.yprm + gyprm*gy
    ezca['SUS-IM4_M1_OPTICALIGN_Y_OFFSET']  = ref.yim4 + gyim4*gy
    ezca['SUS-SR2_M1_OPTICALIGN_Y_OFFSET']  = ref.ysr2 + gysr2*gy
    ezca['SUS-SRM_M1_OPTICALIGN_Y_OFFSET']  = ref.ysrm + gysrm*gy
    ezca['SUS-ETMX_M0_OPTICALIGN_Y_OFFSET'] = ref.yex  + gyex *gy
    ezca['SUS-ETMY_M0_OPTICALIGN_Y_OFFSET'] = ref.yey  + gyey *gy
    ezca['SUS-ITMX_M0_OPTICALIGN_Y_OFFSET'] = ref.yix  + gyix *gy
    ezca['SUS-ITMY_M0_OPTICALIGN_Y_OFFSET'] = ref.yiy  + gyiy *gy
    #
    return True

def pr2spotmove(ref):
    pitPR3toPR2=-9.2;
    yawPR3toPR2=+9.2;
    pitPR3toIM4=56;
    yawPR3toIM4=11;
    pitPR3toPRM=1.5;
    yawPR3toPRM=2.2;
    #
    p3=ezca['SUS-PR3_M1_OPTICALIGN_P_OFFSET']
    y3=ezca['SUS-PR3_M1_OPTICALIGN_Y_OFFSET']
    ezca['SUS-PR2_M1_OPTICALIGN_P_OFFSET'] = ref.ppr2 + pitPR3toPR2*(p3-ref.ppr3)
    ezca['SUS-IM4_M1_OPTICALIGN_P_OFFSET'] = ref.pim4 + pitPR3toIM4*(p3-ref.ppr3)
    ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET'] = ref.pprm + pitPR3toPRM*(p3-ref.ppr3)
    ezca['SUS-PR2_M1_OPTICALIGN_Y_OFFSET'] = ref.ypr2 + yawPR3toPR2*(y3-ref.ypr3)
    ezca['SUS-IM4_M1_OPTICALIGN_Y_OFFSET'] = ref.yim4 + yawPR3toIM4*(y3-ref.ypr3)
    ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET'] = ref.yprm + yawPR3toPRM*(y3-ref.ypr3)
    return True

def prmspotmove(ref):
    pitIM3toPR2=+0.0022;
    yawIM3toPR2=+0.0055;
    pitIM3toIM4=-1.3;
    yawIM3toIM4=+0.92;
    pitIM3toPRM=-0.009;
    yawIM3toPRM=+0.031;
    #
    pi3=ezca['SUS-IM3_M1_OPTICALIGN_P_OFFSET'];
    yi3=ezca['SUS-IM3_M1_OPTICALIGN_Y_OFFSET'];
    ezca['SUS-PR2_M1_OPTICALIGN_P_OFFSET'] = ref.ppr2 + pitIM3toPR2*(pi3-ref.pim3)
    ezca['SUS-IM4_M1_OPTICALIGN_P_OFFSET'] = ref.pim4 + pitIM3toIM4*(pi3-ref.pim3)
    ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET'] = ref.pprm + pitIM3toPRM*(pi3-ref.pim3)
    ezca['SUS-PR2_M1_OPTICALIGN_Y_OFFSET'] = ref.ypr2 + yawIM3toPR2*(yi3-ref.yim3)
    ezca['SUS-IM4_M1_OPTICALIGN_Y_OFFSET'] = ref.yim4 + yawIM3toIM4*(yi3-ref.yim3)
    ezca['SUS-PRM_M1_OPTICALIGN_Y_OFFSET'] = ref.yprm + yawIM3toPRM*(yi3-ref.yim3)
    return True
