# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_LOCK.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/h1/guardian/SQZ_LO.py $

import sys
import time
import ISC_library
from guardian import GuardState, GuardStateDecorator, NodeManager


##################################################
# NODES
##################################################

nodes = NodeManager(['SQZ_CLF',
                     'SQZ_OPO',
                     'SQZ_LO',
                     ])


nominal = 'LOCKED_CLF'

##################################################
# FUNCTIONS
##################################################
def OPO_LOCKED():
    log('Checking OPO in OPO_LOCKED()')
    flag = False
    if ezca['GRD-SQZ_OPO_STATE_N'] == 17:
        flag = True
    return flag

def IMC_LOCKED():
    log('Checking IMC in IMC_LOCKED()')
    flag = False
    if ezca['GRD-IMC_LOCK_STATE_N'] == 100 or ezca['GRD-IMC_LOCK_STATE_N'] == 70:
        flag = True
    return flag


#############################################
#States
#############################################



class INIT(GuardState):
    index = 0

    def main(self):
         nodes.set_managed()
         return True


class DOWN(GuardState):
    index = 1
    goto = True

    def main(self):
        nodes['SQZ_OPO'] = 'DOWN'
        #nodes['SQZ_PLL'] = 'DOWN'
        nodes['SQZ_CLF'] = 'DOWN'
        nodes['SQZ_LO'] = 'DOWN'
        pass

    def run(self):
        return True

class IDLE(GuardState):
    index = 3
    request = False

    def run(self):
        return True

# Lock with high seed power for IFO alignment 
class LOCKING_SEED_HIGH(GuardState):
    index = 24
    request = False

    @ISC_library.unstall_nodes(nodes)
    def main(self):
        nodes['SQZ_OPO'] = 'LOCKED_SEED_HIGH'

    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if nodes['SQZ_OPO'].arrived and nodes['SQZ_OPO'].done:
            log('OPO locked with SEED high power.')
            return True

# Lock with low seed power for NLG measurement
# Some thing didn't work here (Jan3)
class LOCKING_SEED_LOW(GuardState):
    index = 34
    request = False

    @ISC_library.unstall_nodes(nodes)
    def main(self):
        if ezca['GRD-SQZ_OPO_STATE_N'] != 37 :
            nodes['SQZ_OPO'] = 'LOCKED_SEED_LOW'

    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if nodes['SQZ_OPO'].arrived :
            log('OPO locked with SEED low power.')
            return True


class LOCKING_CLF(GuardState):
    index = 14
    request = False

    @ISC_library.unstall_nodes(nodes)
    def main(self):
        nodes['SQZ_OPO'] = 'LOCKED_CLF_DUAL'
        nodes['SQZ_LO'] = 'DOWN' #This way LO and OPO don't fight over OPO PZT
        self.counter =0
    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if nodes['SQZ_OPO'].arrived and self.counter ==0:
#            time.sleep(3) #allow time for PZT err correction. Feel free to replace with anything better than sleep (NK)
            nodes['SQZ_LO'] = 'ADJUST_FREQUENCY'
            nodes['SQZ_CLF'] = 'LOCKED'
            self.counter +=1
            notify('CLF locked and Frequency adjusted')
        elif self.counter ==1:
            if nodes['SQZ_LO'].arrived and nodes['SQZ_CLF'].arrived:
                return True
        
class LOCKED_SEED_HIGH(GuardState):
    index = 27

    @ISC_library.unstall_nodes(nodes)
    def run(self):
       if ezca['GRD-SQZ_OPO_STATE_N'] == 27:
           return True
       else:
           return 'LOCKING_SEED_HIGH'


class LOCKED_SEED_LOW(GuardState):
    index = 37

    @ISC_library.unstall_nodes(nodes)
    def run(self):
       if ezca['GRD-SQZ_OPO_STATE_N'] == 37:
           return True
       else:
           return 'LOCKING_SEED_LOW'

       
class LOCKED_CLF(GuardState):
    index = 17

    @ISC_library.unstall_nodes(nodes)
    def run(self):
        if OPO_LOCKED():
            return True
        else:
            return 'LOCKING_CLF'



##################################################

edges = [
    ('INIT', 'IDLE'),
    ('INIT', 'DOWN'),
    ('IDLE', 'DOWN'),
    ('DOWN', 'LOCKING_SEED_LOW'),
    ('DOWN', 'LOCKING_SEED_HIGH'),
    ('DOWN', 'LOCKING_CLF'),
    ('LOCKING_SEED_LOW', 'LOCKED_SEED_LOW'),
    ('LOCKING_SEED_HIGH', 'LOCKED_SEED_HIGH'),
    ('LOCKING_CLF', 'LOCKED_CLF'),
]


